﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class NodeManager : EditorWindow {
	
	public LevelNodeData nodeData;

	public List<string> surfaceTags;
	public List<string> volumeTags;
	public List<string> avoidCollisionTags;

	List<Vector3> allPos = new List<Vector3>();

	string sceneName;
	int connectCount = 0;

	[MenuItem ("Custom/NodeEditor")]

	static void Init () {
		NodeManager nm = (NodeManager)EditorWindow.GetWindow (typeof (NodeManager));
		nm.Show();
	}

	void  OnGUI () {

		GUILayout.BeginVertical ();

		if (sceneName != SceneManager.GetActiveScene().name) {

			sceneName = SceneManager.GetActiveScene ().name;

			string objectPath = "Assets/Resources/NodeData/" + sceneName + ".asset";

			nodeData = AssetDatabase.LoadAssetAtPath (objectPath, typeof(LevelNodeData)) as LevelNodeData;
		}

		GUILayout.Label ("Current Scene : " + sceneName, EditorStyles.boldLabel);
		GUILayout.Space(10);

		if (nodeData == null) {
			if (GUILayout.Button ("Create New Node Data", GUILayout.ExpandWidth (false))) {
				CreateNewNodeData ();
			}
		} else {

			ScriptableObject target = this;
			SerializedObject so = new SerializedObject(target);

			SerializedProperty nodeDataObj = so.FindProperty("nodeData");
			SerializedProperty surfTags = so.FindProperty("surfaceTags");
			SerializedProperty volTags = so.FindProperty("volumeTags");
			SerializedProperty avColTags = so.FindProperty("avoidCollisionTags");

			EditorGUILayout.PropertyField(nodeDataObj, true); // True means show children
			EditorGUILayout.PropertyField(surfTags, true); // True means show children
			EditorGUILayout.PropertyField(volTags, true); // True means show children
			EditorGUILayout.PropertyField(avColTags, true); // True means show children
			so.ApplyModifiedProperties(); // Remember to apply modified properties

			GUILayout.Space (10);

			GUILayout.Label ("Total Nodes : " + nodeData.allNodes.Count.ToString(), EditorStyles.label);
			GUILayout.Label ("Total Connections : " + connectCount.ToString(), EditorStyles.label);

			GUILayout.Space (10);

			if (GUILayout.Button ("Clear Node Data", GUILayout.ExpandWidth (false))) {
				ClearNodeData ();
			}

			if (GUILayout.Button ("Create Node Data from Map (Replaces Prev Data)", GUILayout.ExpandWidth (false))) {
				CreateMapNodeData ();
			}
		}

		GUILayout.EndVertical ();

		if (GUI.changed) {
			EditorUtility.SetDirty (nodeData);
		}
	}

	void CreateNewNodeData () {
		nodeData = ScriptableObject.CreateInstance<LevelNodeData>();

		AssetDatabase.CreateAsset(nodeData, "Assets/Resources/NodeData/" + sceneName + ".asset");
		AssetDatabase.SaveAssets();
	}

	void ClearNodeData () {
		nodeData.allNodes.Clear ();
		allPos.Clear ();
		connectCount = 0;
	}

	void CreateMapNodeData () {

		nodeData.allNodes = new List<NodeData> ();
		allPos = new List<Vector3> ();

		List<GameObject> allSurfaces = new List<GameObject> ();
		List<GameObject> volSurfaces = new List<GameObject> ();

		//Getting all gamobjects in the scene
		for (int i = 0; i < surfaceTags.Count; i++) {
			allSurfaces.AddRange (GameObject.FindGameObjectsWithTag(surfaceTags[i]));
		}

		for (int i = 0; i < volumeTags.Count; i++) {
			volSurfaces.AddRange (GameObject.FindGameObjectsWithTag(volumeTags[i]));
		}

		//Making Node Data from surface gameobjects
		for (int i = 0; i < allSurfaces.Count; i++) {
			Vector3 pos = allSurfaces [i].transform.position;
			Vector3 sc = allSurfaces [i].transform.lossyScale;
			RaycastHit hit;
			Vector3 start = pos - ((sc / 2f) - new Vector3(0.5f, 0.5f, 0.5f));

			for(int x = 0; x < (int)sc.x; x++) {
				for(int z = 0; z < (int)sc.z; z++) {
					Ray ray = new Ray (start + new Vector3 (x, sc.y - 1f, z), Vector3.up);
					bool isWalkable = true;

					if (Physics.Raycast (ray, out hit, 1f)) {
						if (!avoidCollisionTags.Contains(hit.collider.tag)) {
							isWalkable = false;
						}
					}

					Vector3 nodePos = start + new Vector3 (x, sc.y - 1f, z);
					NodeData n = new NodeData (nodePos, isWalkable);
					nodeData.allNodes.Add (n);
					allPos.Add (nodePos);
				}	
			}
		}

		//Making Node from trigger surfaces gameobjects
		for (int i = 0; i < volSurfaces.Count; i++) {
			Vector3 pos = volSurfaces [i].transform.position;
			Vector3 sc = volSurfaces [i].transform.lossyScale;
			RaycastHit hit;
			Vector3 start = pos - ((sc / 2f) - new Vector3(0.5f, 0.5f, 0.5f));

			for(int x = 0; x < (int)sc.x; x++) {
				for (int y = 0; y < (int)sc.y; y++) {
					for (int z = 0; z < (int)sc.z; z++) {
						Ray ray = new Ray (start + new Vector3 (x, y, z), Vector3.up);
						bool isWalkable = true;

						if (Physics.Raycast (ray, out hit, 1f)) {
							if (!avoidCollisionTags.Contains(hit.collider.tag)) {
								isWalkable = false;
							}
						}

						Vector3 nodePos = start + new Vector3 (x, y, z);
						NodeData n = new NodeData (nodePos, isWalkable);
						nodeData.allNodes.Add (n);
						allPos.Add (nodePos);
					}
				}
			}
		}

		MakeConnections ();
	}

	void MakeConnections () {
		connectCount = 0;

		foreach (NodeData node in nodeData.allNodes) {
			if (allPos.Contains (node.pos + new Vector3 (1f, 0f, 0f))) {
				node.connectionsIndex.Add (allPos.IndexOf(node.pos + new Vector3 (1f, 0f, 0f)));
				connectCount++;
			}

			if (allPos.Contains (node.pos + new Vector3 (-1f, 0f, 0f))) {
				node.connectionsIndex.Add (allPos.IndexOf(node.pos + new Vector3 (-1f, 0f, 0f)));
				connectCount++;
			}

			if (allPos.Contains (node.pos + new Vector3 (0f, 1f, 0f))) {
				node.connectionsIndex.Add (allPos.IndexOf(node.pos + new Vector3 (0f, 1f, 0f)));
				connectCount++;
			}

			if (allPos.Contains (node.pos + new Vector3 (0f, -1f, 0f))) {
				node.connectionsIndex.Add (allPos.IndexOf(node.pos + new Vector3 (0f, -1f, 0f)));
				connectCount++;
			}

			if (allPos.Contains (node.pos + new Vector3 (0f, 0f, 1f))) {
				node.connectionsIndex.Add (allPos.IndexOf(node.pos + new Vector3 (0f, 0f, 1f)));
				connectCount++;
			}

			if (allPos.Contains (node.pos + new Vector3 (0f, 0f, -1f))) {
				node.connectionsIndex.Add (allPos.IndexOf(node.pos + new Vector3 (0f, 0f, -1f)));
				connectCount++;
			}
		}
	}
}
