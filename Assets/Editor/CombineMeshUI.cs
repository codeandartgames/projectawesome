﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NodeDataGizmo))]
public class NodeGizmoUI : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		NodeDataGizmo myScript = (NodeDataGizmo)target;
		if(GUILayout.Button("Refresh Node Data"))
		{
			myScript.GetNodeData();
		}
	}
}
