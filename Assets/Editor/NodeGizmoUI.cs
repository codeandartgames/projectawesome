﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MeshCombine))]
public class CombineMeshUI : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		MeshCombine myScript = (MeshCombine)target;
		if(GUILayout.Button("Combine Children Meshes"))
		{
			myScript.CombineMeshes ();
		}
	}
}
