﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class MeshCombine : MonoBehaviour {

	[SerializeField]
	Material newMeshMat;

	public void CombineMeshes () {

		//resetExistingmesh
		MeshFilter meshFilter = GetComponent<MeshFilter>();
		meshFilter.sharedMesh = new Mesh ();

		MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combine = new CombineInstance[meshFilters.Length];
		int i = 0;
		while (i < meshFilters.Length) {
			combine[i].mesh = meshFilters[i].sharedMesh;
			combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
			if(meshFilters[i].transform != transform)
				meshFilters [i].GetComponent<MeshRenderer> ().enabled = false;
			i++;
		}

		meshFilter.sharedMesh = new Mesh();
		meshFilter.sharedMesh.CombineMeshes(combine);
		transform.GetComponent<MeshRenderer> ().material = newMeshMat;
	}
}