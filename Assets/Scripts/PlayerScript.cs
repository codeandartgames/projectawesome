﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//using UnityEditor;

public class PlayerScript : MonoBehaviour {

	public enum PlayerState {Idle, Moving, Anim};
	public PlayerState playerState = PlayerState.Anim;

	public static bool isEditorCube = false;
	public EditorCube currEditorCube;

	List<NodeData> map = new List<NodeData>();

	[SerializeField]
	float maxSpeed;
	public float speed = 0f;
	public Vector3 dir;
	List<Vector3> pathPoints = new List<Vector3>();
	public static AStar pathFinder;

	[SerializeField]
	MoveMarker moveMarker;
	Vector3 targetPos;
	int targetIndex = 0;

	[SerializeField]
	Transform startNode = null, endNode = null;
	bool isEndLevel = false;

	[SerializeField]


	// Use this for initialization
	void Start () {
		string objectPath = "NodeData/" + SceneManager.GetActiveScene ().name;
		LevelNodeData savedData = Resources.Load (objectPath, typeof(LevelNodeData)) as LevelNodeData;

		if (savedData) {
			LevelNodeData m = Object.Instantiate (savedData) as LevelNodeData;
			map = m.allNodes;
		}

		pathFinder = new AStar (map);

		maxSpeed *= 3f;
		SetPathPoints (new List<Vector3>(){startNode.position});
		//playerState = PlayerState.Idle;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0) && (playerState != PlayerState.Anim) && !MovableCube.isDragged) {
			playerState = PlayerState.Idle;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider) {

					bool isValid = false;

					if (!isEditorCube) {
						isValid = true;
					} else {
						if (hit.collider.CompareTag ("MovableCube")) {
							isValid = false;
						} else {
							isValid = true;
						}
					}

					if (isValid) {
						NodeData target = GetNearestWalkableNode (hit.point);
						NodeData start = GetNearestWalkableNode (transform.position);

						if(moveMarker)
							moveMarker.SetClickPos (target.pos);

						playerState = PlayerState.Idle;
						pathFinder.FindPath (start, target);
						SetPathPoints (pathFinder.GetPath ());
					}
				}	
			}
		}

		if (playerState == PlayerState.Moving || playerState == PlayerState.Anim) {
			if (Vector3.Distance (transform.position, pathPoints [targetIndex]) <= 0.1f) {
				if (pathPoints.Count > (targetIndex + 1)) {
					targetIndex++;
					targetPos = pathPoints [targetIndex];
					dir = Vector3.Normalize(targetPos - transform.position);
				} else {
					PlayerStop ();
				}
			}

			speed = Mathf.Lerp (speed, maxSpeed, Time.deltaTime * 3f);
		} else {
			speed = Mathf.Lerp (speed, 0f, Time.deltaTime * 3f);
		}

		transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * speed);
	}

	void SetPathPoints (List<Vector3> p) {
		pathPoints.Clear ();
		pathPoints = p;

		if (pathPoints.Count > 0) {

			if (isEditorCube) {
				isEditorCube = false;
				currEditorCube.OnEditorCube (false);
			}

			for (int i = 0; i < pathPoints.Count; i++) {
				pathPoints [i] += new Vector3 (0f, 0.5f + (transform.localScale.y / 2f), 0f);
			}
				
			if (playerState != PlayerState.Anim) {
				playerState = PlayerState.Moving;
			}

			targetIndex = 0;
			targetPos = pathPoints [targetIndex];
			dir = Vector3.Normalize(targetPos - transform.position);
		}
	}

	NodeData GetNearestWalkableNode (Vector3 pos) {
		float x = Time.time;
		float min = Mathf.Infinity;
		int minIndex = 0;

		for (int i = 0; i < map.Count; i++) {
			float dist = Vector3.Distance (pos, map [i].pos);

			if (dist < min && map[i].isWalkable) {
				minIndex = i;
				min = dist;
			}
		}

		return map [minIndex];
	}

	void PlayerStop () {

		if (playerState == PlayerState.Anim) {
			if (!isEndLevel) {
				maxSpeed /= 3f;
			} else {
				//Next Scene
			}
		}

		if (!isEndLevel) {
			playerState = PlayerState.Idle;
			GameObject editorCube = StopCheck ("EditorCube");

			if (editorCube) {
				currEditorCube = editorCube.GetComponent<EditorCube> ();
				currEditorCube.OnEditorCube (true);
				isEditorCube = true;
			}
		}
	}

	GameObject StopCheck (string tag) {

		Ray ray = new Ray (transform.position, Vector3.down);
		RaycastHit hit;

		if(Physics.Raycast(ray, out hit)) {
			if(hit.collider.CompareTag(tag)) {
				return hit.collider.gameObject;
			}
		}

		return null;
	}

	void OnTriggerEnter (Collider col) {
		if (col.gameObject.CompareTag("EndLevelCube")) {
			playerState = PlayerState.Anim;
			isEndLevel = true;
			maxSpeed *= 3f;
			SetPathPoints (new List<Vector3>(){endNode.position});
		}
	}
}
