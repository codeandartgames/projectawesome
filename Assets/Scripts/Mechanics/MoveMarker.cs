﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMarker : MonoBehaviour {

	[SerializeField]
	float nodeHeight;

	[SerializeField]
	SpriteRenderer sr;

	Queue<Vector3> allClickPos = new Queue<Vector3>();
	bool isActive = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetClickPos (Vector3 pos) {
		allClickPos.Enqueue (pos + new Vector3 (0f, nodeHeight * 0.52f, 0f));

		if (!isActive) {
			StartCoroutine ("ClickAnim");
		}
	}

	IEnumerator ClickAnim () {

		isActive = true;
		if (allClickPos.Count > 0) {
			Vector3 pos = allClickPos.Dequeue ();
			transform.position = pos;

			float alpha = 0f;

			while (alpha <= 1f) {
				alpha += 0.1f;
				sr.color = new Color (sr.color.r, sr.color.g, sr.color.b, alpha);
				yield return null;
			}

			while (alpha > 0f) {
				alpha -= 0.1f;
				sr.color = new Color (sr.color.r, sr.color.g, sr.color.b, alpha);
				yield return null;
			}

			alpha = 0f;
			sr.color = new Color (sr.color.r, sr.color.g, sr.color.b, alpha);
		}

		if (allClickPos.Count > 0) {
			StartCoroutine ("ClickAnim");
		} else {
			isActive = false;
		}
	}
}
