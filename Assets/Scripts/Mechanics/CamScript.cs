﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour {

	float followSpeed = 3f;

	[SerializeField]
	PlayerScript player;

	static Vector3 camShift = Vector3.zero;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (player.playerState != PlayerScript.PlayerState.Anim) {
			Vector3 targetPos = player.transform.position + new Vector3 (3f, 0f, 0f) + camShift;
			transform.position = Vector3.Lerp (transform.position, targetPos, followSpeed * Time.deltaTime);
		}

		Quaternion rot = Quaternion.Euler (45f - ((player.speed / 2f) * player.dir.x) , 45f - ((player.speed / 2f) * player.dir.z), 0f);
		transform.rotation = Quaternion.Lerp (transform.rotation, rot, Time.deltaTime * 1f);
	}

	public static void SetCamShift (Vector3 shiftPos) {
		camShift = shiftPos;
	}
}
