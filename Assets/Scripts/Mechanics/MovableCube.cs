﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableCube : MonoBehaviour {

	bool isMoveActive = false;
	Transform targetPlatform = null;

	Vector3 screenSpace, curPosition, oldPosition;

	bool isSnapped = false;

	Material selfMat;

	[SerializeField]
	BoxCollider bc;

	Color defaultColor, targetColor;
	[SerializeField]
	Color moveCubesColor;

	public static bool isDragged = false;

	// Use this for initialization
	void Start () {
		selfMat = GetComponent<MeshRenderer> ().material;
		defaultColor = selfMat.color;
		oldPosition = transform.position;
	}

	public void SetMoveStatus (bool isMove) {
		if (isMove) {
			targetColor = moveCubesColor;
			isMoveActive = true;
		} else {
			targetColor = defaultColor;
			isMoveActive = false;
		}

		StopCoroutine ("ChangeColor");
		StartCoroutine ("ChangeColor");
	}

	IEnumerator ChangeColor () {
		float t = 0f;

		while (t <= 1f) {
			t += 0.01f;

			selfMat.color = Color.Lerp (selfMat.color, targetColor, t);

			yield return null;
		}
	}

	void OnMouseDown () {
		if (isMoveActive) {
			bc.enabled = false;
			isDragged = true;
			screenSpace = Camera.main.WorldToScreenPoint (transform.position);
		}
	}

	void OnMouseDrag () {
		if (isMoveActive) {
			Vector3 curScreenSpace = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
			curPosition = Camera.main.ScreenToWorldPoint (curScreenSpace);

			RaycastHit hit;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
				if (hit.collider.tag != "Player") {
					targetPlatform = hit.collider.transform;
				} else {
					targetPlatform = null;
				}
			} else {
				targetPlatform = null;
			}

			//all possible planes conditions
			if (targetPlatform != null) {
				isSnapped = true;
				curPosition = GetCubeSnapPosition (targetPlatform.position, targetPlatform.lossyScale, hit);
			} else {
				isSnapped = false;
			}

			transform.position = curPosition;
		} else {
			transform.position = oldPosition;
		}
	}

	void OnMouseUp () {
		if (isMoveActive) {
			isDragged = false;
			bc.enabled = true;

			if (!isSnapped) {
				curPosition = oldPosition;
			} else {
				PlayerScript.pathFinder.UpdateMap(oldPosition, curPosition);
				transform.SetParent (targetPlatform);
				oldPosition = curPosition;
			}

			transform.position = curPosition;
		} else {
			transform.position = oldPosition;
		}
	}

	Vector3 GetCubeSnapPosition (Vector3 pos, Vector3 sc, RaycastHit hit) {

		Vector3 targetPos = Vector3.zero;

		Vector3 diff = hit.point - pos;

		if (sc.x % 2 != 0) {
			diff.x = Mathf.RoundToInt (diff.x);
		} else {
			diff.x = Mathf.Sign(diff.x) * (Mathf.Abs((int)diff.x) + 0.5f);
		}

		if (sc.y % 2 != 0) {
			diff.y = Mathf.RoundToInt (diff.y);
		} else {
			diff.y = Mathf.Sign(diff.y) * (Mathf.Abs((int)diff.y) + 0.5f);
		}

		if (sc.z % 2 != 0) {
			diff.z = Mathf.RoundToInt (diff.z);
		} else {
			diff.z = Mathf.Sign(diff.z) * (Mathf.Abs((int)diff.z) + 0.5f);
		}

		if (hit.normal == new Vector3 (0f, 1f, 0f)) {
			targetPos = new Vector3 (pos.x + diff.x, hit.point.y + 0.5f, pos.z + diff.z);
		}
		else if (hit.normal == new Vector3 (0f, -1f, 0f)) {
			targetPos = new Vector3 (pos.x + diff.x, hit.point.y - 0.5f, pos.z + diff.z);
		}
		else if (hit.normal == new Vector3 (1f, 0f, 0f)) {
			targetPos = new Vector3 (hit.point.x + 0.5f, pos.y + diff.y, pos.z + diff.z);
		}
		else if (hit.normal == new Vector3 (-1f, 0f, 0f)) {
			targetPos = new Vector3 (hit.point.x - 0.5f, pos.y + diff.y, pos.z + diff.z);
		}
		else if (hit.normal == new Vector3 (0f, 0f, 1f)) {
			targetPos = new Vector3 (pos.x + diff.x, pos.y + diff.y, hit.point.z + 0.5f);
		}
		else if (hit.normal == new Vector3 (0f, 0f, -1f)) {
			targetPos = new Vector3 (pos.x + diff.x, pos.y + diff.y, hit.point.z - 0.5f);
		}

		return targetPos;
	}
}
