﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneKeeper : MonoBehaviour {
		
	private static SceneKeeper _instance;
	public static SceneKeeper Instance { get { return _instance; } }

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}
}
