﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EditorCube : MonoBehaviour {
	[SerializeField]
	List<MovableCube> moveCubes = new List<MovableCube> ();

	public Vector3 camShift;
	public Vector3 camEulerAngles;

	public void OnEditorCube (bool isActiveMoveCubes) {
		for (int i = 0; i < moveCubes.Count; i++) {
			moveCubes [i].SetMoveStatus (isActiveMoveCubes);
		}

		if (isActiveMoveCubes) {
			CamScript.SetCamShift (camShift);
		} else {
			CamScript.SetCamShift (Vector3.zero);
		}
	}
}

