﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NodeDataGizmo : MonoBehaviour {

	LevelNodeData nodeData;

	void Start () {
		GetNodeData ();
	}

	void OnEnable () {
		GetNodeData ();
	}

	public void GetNodeData () {
		string objectPath = "NodeData/" + SceneManager.GetActiveScene ().name;
		nodeData = Resources.Load (objectPath, typeof(LevelNodeData)) as LevelNodeData;
	}

	void OnDrawGizmosSelected() {
		if (nodeData) {
			for (int i = 0; i < nodeData.allNodes.Count; i++) {
				if (nodeData.allNodes [i].isWalkable) {
					Gizmos.color = new Color (0, 1, 0, 0.5f);
				} else {
					Gizmos.color = new Color (1, 0, 0, 0.5f);
				}

				Gizmos.DrawCube (nodeData.allNodes [i].pos, new Vector3 (1, 1, 1));
			}
		}
	}
}
