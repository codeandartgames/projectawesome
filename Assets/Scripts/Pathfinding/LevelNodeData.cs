﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Pathfinding/NodeGraph")]
public class LevelNodeData : ScriptableObject {

	public List<NodeData> allNodes = new List<NodeData> ();

}
