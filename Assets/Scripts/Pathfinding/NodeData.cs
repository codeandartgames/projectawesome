﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NodeData {
	
	public Vector3 pos;

	public bool isWalkable;

	public List<int> connectionsIndex = new List<int> ();

	public NodeData() {
		
	}

	public NodeData (Vector3 pos, bool isWalkable) {
		this.pos = pos;
		this.isWalkable = isWalkable;
	}
}
