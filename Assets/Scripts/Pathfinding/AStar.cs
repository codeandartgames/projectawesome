﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AStar {

	class Node {
		public Vector3 pos;
		public float G;
		public float H;
		public float F;
		public Node parent;
		public NodeData nodeData;

		public Node (Vector3 pos, float G, float F, float H, Node parent, NodeData n) {
			this.pos = pos;
			this.G = G;
			this.H = H;
			this.F = F;
			this.parent = parent;
			this.nodeData = n;
		}
	}

	List<Node> openList;
	List<Node> closeList;
	List<Node> neighbours;
	List<Node> finalPath;

	Node start;
	Node end;

	List<NodeData> map;

	public AStar (List<NodeData> map) {
		openList = new List<Node>();
		closeList = new List<Node>();
		neighbours = new List<Node>();
		finalPath = new List<Node>();
		this.map = map;
	}

	public void FindPath(NodeData st, NodeData target) {
		ResetLists ();
		start = new Node(st.pos, 0, 0, 0, null, st);
		end = new Node(target.pos, 0, 0, 0, null, target);

		openList.Add(start);

		bool keepSearching = true;
		bool pathExists = true;

		while ((keepSearching) && (pathExists)) {
			Node currentNode = ExtractBestNodeFromOpenList();
			if (currentNode == null) {
				pathExists = false;
				break;
			}
			closeList.Add(currentNode);
			if (NodeIsGoal(currentNode))
				keepSearching = false;
			else {
				FindValidNeighbours(currentNode);

				foreach (Node neighbour in neighbours) {
					if (FindInCloseList(neighbour) != null)
						continue;
					Node inOpenList = FindInOpenList(neighbour);
					if (inOpenList == null) {
						openList.Add (neighbour);
					}
					else {
						if (neighbour.G < inOpenList.G) {
							inOpenList.G = neighbour.G;
							inOpenList.F = inOpenList.G + inOpenList.H;
							inOpenList.parent = currentNode;
						}
					}   
				}
			}
		}

		if (pathExists) {
			Node n = FindInCloseList (end);
			while (n != null) {
				finalPath.Add (n);
				n = n.parent;
			}
		}
	}

	void ResetLists () {
		openList = new List<Node>();
		closeList = new List<Node>();
		neighbours = new List<Node>();
		finalPath = new List<Node>();
	}

	public List<Vector3> GetPath () {
		List<Vector3> path = new List<Vector3> ();

		foreach (Node n in finalPath) {
			path.Add(n.pos);   
		}

		if (path.Count != 0) {
			path.Reverse ();
			path.RemoveAt (0);
		}

		return path;
	}

	Node ExtractBestNodeFromOpenList() {
		float minF = float.MaxValue;
		Node bestOne = null;

		foreach (Node n in openList) {
			if (n.F < minF) {
				minF = n.F;
				bestOne = n;
			}
		}

		if (bestOne != null)
			openList.Remove(bestOne);
		
		return bestOne;
	}

	bool NodeIsGoal(Node node) {
		return (node.pos == end.pos);
	}

	void FindValidNeighbours(Node n) {
		neighbours.Clear();

		List<int> connIndex = n.nodeData.connectionsIndex;

		foreach (int i in connIndex) {
			if (map[i].isWalkable) {
				Node vn = PrepareNewNodeFrom (n, map[i]);
				neighbours.Add (vn);
			}
		}
	}

	Node PrepareNewNodeFrom(Node n, NodeData nd) {
		Node newNode = new Node(nd.pos, 0, 0, 0, n, nd);
		newNode.G = n.G + MovementCost(n, newNode);
		newNode.H = Heuristic(newNode);
		newNode.F = newNode.G + newNode.H;
		newNode.parent = n;
		return newNode;
	}

	float Heuristic (Node n) {
		return Vector3.Distance(end.pos, n.pos);
	}

	float MovementCost(Node a, Node b) {
		return Vector3.Distance(a.pos, b.pos);
	}

	Node FindInCloseList(Node n) {
		foreach (Node nn in closeList) {
			if (nn.nodeData == n.nodeData)
				return nn;
		}
		return null;
	}

	Node FindInOpenList(Node n) {
		foreach (Node nn in openList) {
			if (nn.nodeData == n.nodeData)
				return nn;
		}
		return null;
	}

	public void UpdateMap (Vector3 oldPos, Vector3 newPos) {

		int cubeIndex = 0;

		for (int i = 0; i < map.Count; i++) {
			if (map[i].pos == oldPos) {
				cubeIndex = i;
				break;
			}
		}

		map[cubeIndex].pos = newPos;

		//Refresh Existing Connections
		RefreshNodeList(cubeIndex);

		//Refresh Connections of the Movable Cube
		RefreshNode(cubeIndex);

		//Refresh New Nodes Added to Movable Cube Connections
		RefreshNodeList(cubeIndex);
	}

	void RefreshNodeList (int index) {
		List<int> connIndex = map[index].connectionsIndex;

		foreach (int i in connIndex) {
			RefreshNode (i);
		}
	}

	void RefreshNode (int index) {
		NodeData node = map [index];
		node.connectionsIndex = new List<int> ();

		for (int i = 0; i < map.Count; i++) {
			if (map[i].pos == node.pos + new Vector3(1f, 0f, 0f)) {
				node.connectionsIndex.Add (i);
			}

			if (map[i].pos == node.pos + new Vector3(-1f, 0f, 0f)) {
				node.connectionsIndex.Add (i);
			}

			if (map[i].pos == node.pos + new Vector3(0f, 1f, 0f)) {
				node.connectionsIndex.Add (i);
			}

			if (map[i].pos == node.pos + new Vector3(0f, -1f, 0f)) {
				node.connectionsIndex.Add (i);
			}

			if (map[i].pos == node.pos + new Vector3(0f, 0f, 1f)) {
				node.connectionsIndex.Add (i);
			}

			if (map[i].pos == node.pos + new Vector3(0f, 0f, -1f)) {
				node.connectionsIndex.Add (i);
			}
		}

		Ray ray = new Ray (node.pos, Vector3.up);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, 1f)) {
			if (hit.collider.CompareTag ("Player") || hit.collider.CompareTag ("Ladder") || hit.collider.CompareTag ("EndLevelCube")) {
				node.isWalkable = true;
			} else {
				node.isWalkable = false;
			}
		} else {
			node.isWalkable = true;
		}
	}
}